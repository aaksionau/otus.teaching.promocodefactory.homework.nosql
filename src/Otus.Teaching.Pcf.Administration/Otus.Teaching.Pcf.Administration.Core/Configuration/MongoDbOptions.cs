namespace Otus.Teaching.Pcf.Administration.Core.Configuration
{
    public class MongoDbOptions
    {
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}