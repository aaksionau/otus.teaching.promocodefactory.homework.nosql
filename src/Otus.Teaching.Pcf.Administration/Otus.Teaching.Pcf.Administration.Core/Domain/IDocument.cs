using System;

namespace Otus.Teaching.Pcf.Administration.Core.Domain
{
    public interface IDocument
    {
         Guid Id { get; set; }
    }
}