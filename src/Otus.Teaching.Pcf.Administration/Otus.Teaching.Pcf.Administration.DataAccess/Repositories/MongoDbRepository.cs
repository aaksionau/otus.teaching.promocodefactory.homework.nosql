﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Attributes;
using Otus.Teaching.Pcf.Administration.Core.Configuration;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using MongoDB.Bson;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRepository<T> : IRepository<T> where T: IDocument
    {
        private readonly IOptions<MongoDbOptions> _options;
        private readonly IMongoCollection<T> _collection;

        public MongoDbRepository(IOptions<MongoDbOptions> options)
        {
            this._options = options;
            var client = new MongoClient(options.Value.ConnectionString);
            var database = client.GetDatabase(options.Value.DatabaseName);
            _collection = database.GetCollection<T>(GetCollectionName(typeof(T)));
        }

        private protected string GetCollectionName(Type documentType)
        {
            return ((BsonCollectionAttribute) documentType.GetCustomAttributes(
                    typeof(BsonCollectionAttribute),
                    true)
                .FirstOrDefault())?.CollectionName;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _collection.Find(b => true).ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _collection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).ToListAsync();
        }

        public Task AddAsync(T entity)
        {
            return Task.Run(() => _collection.InsertOneAsync(entity));
        }

        public Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, entity.Id);
            return Task.Run(()=>_collection.FindOneAndReplace(filter, entity));
        }

        public Task DeleteAsync(T entity)
        {
            return Task.Run(()=>_collection.FindOneAndDelete(b=>b.Id == entity.Id));
        }
    }
}